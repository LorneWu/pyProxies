# Introduce
A module base on (ProxyBroker)[https://github.com/constverum/ProxyBroker]
This is a project base on the module proxies.
In order to provide more function for handle proxies.
Enjoy it.
目前尚在測試中

# How to use
主要設計內容為增加對proxy的管理，紀錄曾經使用過得proxies的使用狀況成功與否
1. use getip() to get fresh ip
用getip來取proxy
2. use goodip() and badip() to return the ip and rate it.
用goodip和badip來歸還，返回貯列等待下次被取
同時評價使用狀況，若是fail則要用badip來返回，這樣到達一定條件該proxy就不會在出現
3. use store() to store the proxy u have ever use.And the success and failure record.
若是之後還要在使用可以使用store來紀錄上次使用的情況

目前主要分alive using dead
alive : 可被使用的
using : 正被使用
dead : 已被淘汰
可在setting.ini中編輯相關規則
