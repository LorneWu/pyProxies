#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 14:50:43 2017

@author: lorne
"""
import os
import configparser
import os
import time
from datetime import datetime
config = configparser.ConfigParser()
config.read('proxy_setting.ini')
global all_proxy
global useing_proxy

alivePoint = 0
all_proxy = []
useing_proxy = []
proxy_history={}

timeformat = '%Y%b%d_%I%M%S'
from enum import Enum
class Status(Enum):
    S = 'super'
    A = 'alive'
    U = 'using'
    C = 'cooldown'#fail more than success
    D = 'dead'#more than setting count
class proxies():
    def __init__(self,proxy,country,reation,security,start=datetime.now(),last=datetime.now(),status=Status.A,count_suc=0,count_fail=0):
        self.country = country
        self.security = security
        self.reaction = reation
        self.proxy = proxy
        self.starttime = start
        self.lasttime = last
        self.status = status
        self.count_success = count_suc
        self.count_failure = count_fail
    def getSum(self):
        return self.count_success+self.count_failure   
    def getweight(self):
        return self.count_success-self.count_failure     
    def getSuccessRate(self):
        return self.count_success/(self.count_success+self.count_failure) if self.getSum() != 0 else 0
    def __repr__(self):
        return '<proxies %s,%s,%s,%s,%s,%s,%s,%d,%d>'%(self.proxy,self.country,self.reaction,self.security,
                                                       datetime.strftime(self.starttime,timeformat),datetime.strftime(self.lasttime,timeformat),
                                                       self.status.value,self.count_success,self.count_failure)

    def __str__(self):
        return '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%d\t%d\n'%(self.proxy,self.country,self.reaction,self.security,
                                                       datetime.strftime(self.starttime,timeformat),datetime.strftime(self.lasttime,timeformat),
                                                       self.status.value,self.count_success,self.count_failure)
def str2proxies(string):
    tmp = string.split('\t')
    return proxies(tmp[0],tmp[1],tmp[2],tmp[3],datetime.strptime(tmp[4],timeformat),datetime.strptime(tmp[5],timeformat),Status(tmp[6]),int(tmp[7]),int(tmp[8]))

def getnew():# get new proxy list
    os.system('proxybroker find --types HTTP HTTPS --lvl High --strict -l '+ config.get('proxy','minicount')+' -o'+config.get('file','proxyfile'))        
    refuel()

def refuel():
    global all_proxy
    all_proxy_cache = [i.proxy for i in all_proxy]
    with open(config.get('file','proxyfile'),'r') as f :
        for i in f.readlines():
            s = i.replace('<','').replace('>','').strip()
            tmp = s.split(' ',3)[:-1]+s.split(' ',3)[-1].rsplit(' ',1)
            if tmp[4] not in all_proxy_cache:all_proxy.append(proxies(tmp[4],tmp[1],tmp[2],tmp[3]))

def proxy_init():
    global all_proxy
    if not os.path.exists(config.get('file','proxyData')):
        os.system('proxybroker find --types HTTP HTTPS --lvl High --strict -l '+ config.get('proxy','basecount')+' -o'+config.get('file','proxyfile'))    
        refuel()
    else:
        with open(config.get('file','proxyData'),'r') as f:
            for i in f.readlines():all_proxy.append(str2proxies(i))
    #print(int(config.get('proxy','minicount')))
    while True:
        #print(len(all_proxy))
        if len(all_proxy) < int(config.get('proxy','minicount')):getnew()
        else:break
    proxy_store()
def proxy_store():
    global all_proxy
    with open(config.get('file','proxyData'),'w') as f:
        for i in all_proxy:f.write(str(i))
def proxy_reset():
    global all_proxy
    for i in all_proxy:
        i.status = Status.A
        i.count_failure = 0
        i.count_success = 0
        i.lasttime = datetime.now()
def getip():
    global all_proxy
    while True:
        global init_time
        if time.clock() - init_time > int(config.get('proxy','proxy_fresh_time'))/100 or alivePoint == len(all_proxy):
            getnew()
            init_time=time.clock()
        slic = [i for i in all_proxy if i.status.value == 'alive']
        if len(slic)>int(config.get('proxy','proxy_low_limit')):
            tmp = slic[0]
            if (tmp.getSuccessRate() < float(config.get('proxy','bad_proxy_rate') and tmp.getSum() > int(config.get('proxy','bad_proxy_lesstime')))):
                badip(tmp.proxy)
                all_proxy[all_proxy.index(tmp)].status = Status.D
                continue
            else:
                all_proxy[all_proxy.index(tmp)].status = Status.U
                return tmp.proxy
        else:
            print('getnew')
            getnew()
            return getip()
def getIpIndex(ip):
    global all_proxy
    for i in range(len(all_proxy)):
        if all_proxy[i].proxy == ip:return i
    return len(all_proxy)
            

def badip(ip):
    global init_time
    global all_proxy
    global alivePoint
    p = getIpIndex(ip)
    all_proxy[p].count_failure+=1
    if all_proxy[p].getSuccessRate() < float(config.get('proxy','bad_proxy_rate')) and all_proxy[p].getSum() > float(config.get('proxy','bad_proxy_lesstime')):
        all_proxy[p].status = Status.D
        all_proxy[p].lasttime = datetime.now()
        all_proxy[alivePoint],all_proxy[p] = all_proxy[p] ,all_proxy[alivePoint]
        alivePoint+=1
    else:
        all_proxy[p].status = Status.A
        all_proxy[p].lasttime = datetime.now()
   
def goodip(ip):
    global init_time 
    global all_proxy
    p = getIpIndex(ip)
    all_proxy[p].count_success+=1
    #all_proxy[p].status = Status.A
    all_proxy[p].lasttime = datetime.now()
    #No return
def returnip(ip):
    global init_time 
    global all_proxy
    p = getIpIndex(ip)
    all_proxy[p].count_success+=1
    all_proxy[p].status = Status.A
    all_proxy[p].lasttime = datetime.now()
    all_proxy[-1],all_proxy[p] = all_proxy[p] ,all_proxy[-1]
global init_time
init_time = time.clock()
proxy_init()
print('proxy init')
print('Proxy available : '+str(len(all_proxy)))
#goodip(getip())
#goodip(getip())